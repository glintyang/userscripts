// ==UserScript==
// @name       Wow Web Auction
// @namespace  https://bitbucket.org/glintyang/userscripts/overview
// @version    0.1.1
// @description  enter something useful !
// @include    https://www.battlenet.com.cn/wow/zh/vault/character/auction/*
// @require    http://upcdn.b0.upaiyun.com/libs/jquery/jquery-2.0.0.min.js
// @copyright  2013+, Glintyang
// @updateURL	https://bitbucket.org/glintyang/userscripts/raw/master/wow_web_ah.js
// @downloadURL	https://bitbucket.org/glintyang/userscripts/raw/master/wow_web_ah.js
// ==/UserScript==


//清理无用元素
$('#footer').remove();

var uri = location.pathname;

var faction = uri.replace(/.*(alliance|horde|neutral).*/g,'$1');
var op = uri.replace(/.*(alliance|horde|neutral)\/(.*)/g,'$2');
//console.log(faction);
//console.log(op);


var button_bid = $('<a></a>').attr({'href':'browse?n=&filterId=-1&minLvl=-1&maxLvl=-1&qual=1&start=0&end=200&sort=bid&reverse=false','rel':'np'})
    .append("<span class='arrow'><span class='icon'>购买-竞标</span></span>");

$('#profile-sidebar-menu').find('li').eq(2).append(button_bid);


$('body').append($('<div></div>').attr('id','wow_tools'));
var wt = $('#wow_tools');
wt.css({position:'fixed',width:48,height:128,top:0,right:0,'background-color':'grey'});
wt.append($('<div>test1</div>').attr('id','wow_tools_test1').css({'cursor':'pointer','font-size':'20px'}));
wt.append($('<div>test2</div>').attr('id','wow_tools_test2').css({'cursor':'pointer','font-size':'20px'}));
wt.append($('<div>test3</div>').attr('id','wow_tools_test3').css({'cursor':'pointer','font-size':'20px'}));


$('.profile-sidebar-contents').css({position:'relative'}).append($('<ul></ul>').attr('id','wow_sidebar'));
var ws = $('#wow_sidebar').addClass('profile-sidebar-menu');
ws.css({position:'absolute',top:$('.profile-sidebar-crest').height(),right:0,width:'auto',margin:'14px 9px 0 0',background:'none'});
ws.append($('<li><a href="#" class="" rel="np"><span class="arrow"><span class="icon">测试1</span></span></a></li>').attr('id','wow_sidebar_test1'));
ws.append($('<li><a href="#" class="" rel="np"><span class="arrow"><span class="icon">测试2</span></span></a></li>').attr('id','wow_sidebar_test2'));
ws.append($('<li><a href="#" class="" rel="np"><span class="arrow"><span class="icon">测试3</span></span></a></li>').attr('id','wow_sidebar_test3'));
$('#wow_sidebar_test1').css({'text-align':'right',background:'none'});
$('#wow_sidebar_test2').css({'text-align':'right',background:'none'});
$('#wow_sidebar_test3').css({'text-align':'right',background:'none'});
$('#wow_sidebar_test1').find('a').css({padding:'0 16px 0'});
$('#wow_sidebar_test2').find('a').css({padding:'0 16px 0'});
$('#wow_sidebar_test3').find('a').css({padding:'0 16px 0'});

//$('body').css({'margin-top':-274,'background-position-y':-274});
//$('#service').css({top:274});


var getbids = function(){
    $.ajax({
        url:"https://www.battlenet.com.cn/wow/zh/vault/character/auction/" + faction + "/bids",
        type:'get',
        dataType:'xml',
        success:function(xml){
            var bids = $(xml).find('#bids-active').children('div.table').children('table').children('tbody').children('tr').each(function(){
                console.log($(this).attr('id'));
            });
            //console.log(bids);
        }
    });
};

$('#wow_tools_test1').click(function(){
    getbids();
});


var slidingFunction = function () {
    var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    // 跟随滚动
    var sidebar = $('.profile-sidebar-contents');
    if (sidebar.length > 0) {
        if (scrollTop > 274) {
            if ('static'==sidebar.css('position') || 'relative'==sidebar.css('position')) {
                sidebar.css({position:'fixed',top:0});
            }
        } else {
            if ('fixed'==sidebar.css('position')) {
                sidebar.css({position:'relative',top:0});
            }
        }
    }
};





switch (op)
{
    case 'browse': // ============================购买==============================
        console.log(op+'-购买');
        var tb = $('div.auction-house.browse').children("div.table").children("table");

        var th = tb.children("thead").children("tr").children("th"); // 表头顺序
        th.eq(3).after(th.eq(2)).after(th.eq(5)).after(th.eq(0)).after(th.eq(1)).after(th.eq(4));

        var row = tb.children("tbody").children("tr"); // 表内容顺序

        var bids = {};
        $.ajax({ // 获取已竞标的物品
            url:"https://www.battlenet.com.cn/wow/zh/vault/character/auction/" + faction + "/bids",
            type:'get',
            dataType:'xml',
            success:function(xml){
                var i = 0,n = 0;
                bids = $(xml).find('#bids-active').children('div.table').children('table').children('tbody').children('tr');
                row.each(function(index,r){ // 过滤已竞标的物品,已竞标的物品在竞标结果页面察看
                    n++;
                    var rid = $(r).attr('id');
                    bids.each(function(index,b){
                        var bid = $(b).attr('id');
                        if (rid == bid){
                            i++;
                            b.remove();
                            r.remove();
                            return false;
                        }
                    });
                });
                console.log('共'+n+'件');
                console.log('竞价'+i+'件,已隐藏');
            }
        });
        row.each(function(){
            var tr = $(this).children("td");
            tr.eq(3).after(tr.eq(2)).after(tr.eq(5)).after(tr.eq(0)).after(tr.eq(1)).after(tr.eq(4));
            tr.filter(".options").css({"position":"relative"});
            tr.children(".options a").eq(1).css({"position":"absolute","left":"106px","top":"13px"});
            tr.children(".options a").eq(1).hide();
            tr.filter(".options").mouseenter(function(){
                tr.children(".options a").eq(1).show();
            });
            tr.filter(".options").mouseleave(function(){
                tr.children(".options a").eq(1).hide();
            });
            tr.children(".options a").eq(0).click(function(){ //竞价对话框很烦人,按默认最低竞价,模拟点击对话框竞价按钮,有时候浏览物品过程中其他人竞价导致不能直接竞价,需要手动刷新页面
                $(".dialog .inner .bar button").click();
            });
        });
        break;

    case 'bids': // ============================竞价==============================
        console.log(op+'-竞标');
        //var bids = $('#bids-active');
        var row = $('#bids-active').children('div.table.type-table').children('table').children('tbody').children('tr');
        //console.log(row);
        row.each(function(){
            var tr = $(this).children("td");
            tr.children(".options a").eq(0).click(function(){ //竞价对话框很烦人,按默认最低竞价,模拟点击对话框竞价按钮,有时候浏览物品过程中其他人竞价导致价格升高不能直接竞价,需要手动刷新页面
                $(".dialog .inner .bar button").click();
            });
        });
        break;

    case 'create': // ============================出售==============================
        console.log(op+'-出售');
        break;

    default:
        console.log(uri);
        break;
}

jQuery(window).bind("scroll", slidingFunction);
jQuery(window).bind("resize", slidingFunction);
slidingFunction();